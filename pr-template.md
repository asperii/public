#### What does this PR do?
#### Where should the PR reviewer start?
#### Any background context you want to provide?
#### Which JIRA issues does this PR include?
#### Link to relevant documentation
#### Definition of Done:
- [ ] Is there appropriate logging included?
- [ ] Is there appropriate test coverage?
- [ ] Does this PR require a regression test? (All fixes require a regression test.)
#### What gif best describes this PR or how it makes you feel?